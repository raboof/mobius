// Actual dimnsions of my 3 planks.
l = 216;
w = 68;
h = 12;

// TODO eyeballed
m = 51;

module plank() {
    // TODO 0.6 eyeballed
    rotate([0, atan(h/l)+0.6, 0]) translate([-l/2, -w/2-m, -h/2]) cube([l, w, h]);
};

module planken() {
  plank();
  rotate([0, 0, 360/3]) plank();
  rotate([0, 0, 2*360/3]) plank();
};


module zaag() {
  // TODO 101.6 eyeballed
  translate([-w, 101.6, -w/2]) cube([2*w, w, w]);
};

module zagen() {
  zaag();
  rotate([0, 0, 360/3]) zaag();
  rotate([0, 0, 2*360/3]) zaag();
};

module schuur() {
  translate([-w, 90, 0]) difference() {
    translate([0, 0, -1.1*h]) cube([2*w, 2*h, 2.2*h]);
    rotate([0, 90, 0]) cylinder(h=2*w, r=h);
  }
}

module schuren() {
  schuur();
  rotate([0, 0, 360/3]) schuur();
  rotate([0, 0, 2*360/3]) schuur();
};

difference() {
  planken();
  zagen();
  schuren();
}